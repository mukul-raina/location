require('rootpath')();
var should = require('should');
var supertest = require('supertest');
var app = require('server.js');
var api = supertest('http://localhost:8080');

describe('GET /search', function(){
  this.timeout(10000);
  it('Should make a GET request to /search and should get combinatin of yelp and foursquare data back', function(done){
    api.get("/search?zip_code=92801&&limit=20&&offset=0")
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end(function(err, res){
        (err==null).should.be.true();
        res.body.should.be.an.Array();

        done();
      });
  });
});
