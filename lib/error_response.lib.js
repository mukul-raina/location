var zipCodeNotFound = new Error();
zipCodeNotFound.response = {
  "status_code": 400,
  "status": "Bad Request",
  "error_message": "Sorry zip code not found, please try again"
};

var limitNotFound = new Error();
limitNotFound.response = {
  "status_code": 400,
  "status": "Bad Request",
  "error_message": "Sorry pagination limit not found, please try again"
};

var offsetNotFound = new Error();
offsetNotFound.response = {
  "status_code": 400,
  "status": "Bad Request",
  "error_message": "Sorry pagination offset not found, please try again"
};

module.exports.zipCodeNotFound = zipCodeNotFound;
module.exports.limitNotFound = limitNotFound;
module.exports.offsetNotFound = offsetNotFound;

