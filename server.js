// Import modules
require('rootpath')();
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

// config file
var config = require('env.json')[process.env.NODE_ENV || "development"];

// Connect to the db
mongoose.connect(config.DB_URL);
mongoose.connection.on('error', function() {
  console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
});

// Controllers
var searchCtrl = require('controllers/search.controller.js');

// Initialize app
var app = express();

// Initialize different middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Configure express to server static files from the /public directory.
app.use(express.static('public'));

var router = express.Router();

// routes
router.get('/search', searchCtrl.validateQueryParams, searchCtrl.getRestuaruants);

// Apply the routes to our app
app.use('/', router);

// Error handling middleware - sends error message to the client
app.use(function(err, req, res, next){
  console.log(err);
  var defaultResponse = {
    "status_code": "500",
    "status": "Internal Server Error",
    "error_message": ErrorMessage.serverError
  }
  if(err.response){
    res.status(err.response.status_code);
    return res.send(err.response);
  }
  else{
    res.status(defaultResponse.status_code);
    return res.send(defaultResponse);
  }
});

// Start the server
app.listen(process.env.OPENSHIFT_NODEJS_PORT || 8080, process.env.OPENSHIFT_NODEJS_IP);

module.exports.app = app;
