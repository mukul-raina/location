require('rootpath')();
var config = require('env.json')[process.env.NODE_ENV || "development"];
var Yelp = require('yelp');
var foursquare = (require('foursquarevenues'))(config.FOURSQUARE.CLIENTIDKEY, config.FOURSQUARE.CLIENTSECRETKEY);
var YelpModel = require('models/yelp.model.js').yelps;
var FoursuareModel = require('models/foursquare.model.js').foursquares;
var LocationModel = require('models/data.model.js').datas;
var async = require('async');
var request = require('request');

function saveYelpRestaurantToMongo(business, callback){
  var yelp = new YelpModel();
  if(business.hasOwnProperty('name')) yelp.name = business.name;
  if(business.hasOwnProperty('url')) yelp.url = business.url;
  if(business.hasOwnProperty('phone')) yelp.phone = business.phone;
  if(business.hasOwnProperty('id')) yelp.id = business.id;
  if(business.hasOwnProperty('location')){
    yelp.location = {};
    if(business.location.hasOwnProperty('postal_code')) yelp.location.zip_code = business.location.postal_code;
    if(business.location.hasOwnProperty('display_address')) yelp.location.formattedAddress = business.location.display_address;
    if(business.location.hasOwnProperty('coordinate')){
      if(business.location.coordinate.hasOwnProperty('latitude')) yelp.location.latitude = Math.round(business.location.coordinate.latitude * 100) / 100;
      if(business.location.coordinate.hasOwnProperty('longitude')) yelp.location.longitude = Math.round(business.location.coordinate.longitude * 100) / 100;
    }
  };

  yelp.save(function(err, yelpDocument){
    if(err) return callback(err);
    return callback(null, yelpDocument);
  });
};

function saveFoursquareVenueToMongo(venue, callback){
  var foursquare = new FoursuareModel();
  if(venue.hasOwnProperty('id')) foursquare.id = venue.id;
  if(venue.hasOwnProperty('name')) foursquare.name = venue.name;
  if(venue.hasOwnProperty('location')){
    foursquare.location = {};
    if(venue.location.hasOwnProperty('postalCode')) foursquare.location.zip_code = venue.location.postalCode;
    if(venue.location.hasOwnProperty('lat')) foursquare.location.latitude = Math.round(venue.location.lat * 100) / 100;
    if(venue.location.hasOwnProperty('lng')) foursquare.location.longitude = Math.round(venue.location.lng * 100) / 100;
    if(venue.location.hasOwnProperty('formattedAddress')) foursquare.location.formattedAddress = venue.location.formattedAddress;
  }

  foursquare.save(function(err, foursquareDocument){
    if(err) return callback(err);
    return callback(null, foursquareDocument);
  });
}

function getFoursquareRestuaruants(limit, callback){
  var params = {
    "ll": "33.837430,-117.956203",
    // "query": "restaurants",
    "limit": limit,
    "intent": "browse",
    "radius": 10000
  };
  foursquare.getVenues(params, function(err, data) {
    if(err) return callback(err);
    
    async.map(data.response.venues, saveFoursquareVenueToMongo, function(err, result){
      if(err) return callback(err);

      return callback(null, result);
    });
  });
};

function getYelpRestuaruants(zipCode, limit, offset, callback){
  var yelp = new Yelp({
    consumer_key: config.YELP.consumer_key,
    consumer_secret: config.YELP.consumer_secret,
    token: config.YELP.token,
    token_secret: config.YELP.token_secret
  });

  var searchTerms = {
    // 'term': 'restaurants',
    'limit': limit,
    'offset': offset,
    'sort': 1,
    'location': zipCode,
    'cll': "33.837430,-117.956203",
    // "category_filter": 'restaurants',
    "radius_filter": 10000
  }
  yelp.search(searchTerms)
    .then(function (data) {
      var businesses = data.businesses;
      async.map(businesses, saveYelpRestaurantToMongo, function(err, result){
        if(err) return callback(err);

        return callback(null, result);
      });
    })
    .catch(function (err) {
      return callback(err);
    });
};

function saveCombinedDataToMongo(foursquareData, callback){
  var combinedData = new LocationModel();
  if(foursquareData.hasOwnProperty('name')) combinedData.name = foursquareData.name;
  if(foursquareData.hasOwnProperty('location')){
    if(foursquareData.location.hasOwnProperty('zip_code')) combinedData.location.zip_code = foursquareData.location.zip_code;
    if(foursquareData.location.hasOwnProperty('latitude')) combinedData.location.latitude = foursquareData.location.latitude;
    if(foursquareData.location.hasOwnProperty('longitude')) combinedData.location.longitude = foursquareData.location.longitude;
    if(foursquareData.location.hasOwnProperty('formattedAddress')) combinedData.location.formattedAddress = foursquareData.location.formattedAddress;
  }
  request("https://api.what3words.com/position?key=EY26OP1T&lang=en&position="+ combinedData.location.latitude + "," + location.location.latitude, function (error, response, body) {
    if(err) return callback(err);

    combinedData.location.threeWordLocation = body.words[0] + " , " + body.words[1] + " , " + body.words[2];
    location.save(function(err, locationDocument){
      if(err) return callback(err);
      return callback(null, locationDocument);
    });
  });
}

function combineFoursquareAndYelpData(callback){
  YelpModel.find({}, function(err, yelpData){
    if(err) return callback(err);

    // Save the records from yelp in a hash 
    // (key, value) -> (name, index in array)
    var yelpHash = {};
    for(var i = 0; i < yelpData.length; i++){
      yelpHash[yelpData[i].name] = i;
    }

    // Iterate through the foursquare data and see if the name is present in yelp's hash
    // If it is, then that means its a matching record, save it in the combined collection
    FoursuareModel.find({}, function(err, foursquareData){
      if(err) return callback(err);

      var matchingRecords = [];

      for(var i = 0; i < foursquareData.length; i++){
        if(yelpHash.hasOwnProperty(foursquareData[i].name)){
          matchingRecords.push(foursquareData[i]);
        }
      }

      async.map(matchingRecords, saveCombinedDataToMongo, function(err, combinedData){
        if(err) return callback(err);

        return callback(null, combinedData);
      });
    });
  });
}

function getRestuaruants(req, res, next){
  getYelpRestuaruants(req.query.zip_code, req.query.limit, req.query.offset, function(err, data){
    if(err) return next(err);
      
    getFoursquareRestuaruants(req.query.limit, function(err, data){
      if(err) return next(err);

      combineFoursquareAndYelpData(function(err, data){
        if(err) return next(err);

        return res.send(data);
      });  
    });
  });
};

function validateQueryParams(req, res, next){
  if(!req.query.hasOwnProperty('zip_code')) return next(ErrorResponse.zipCodeNotFound);
  if(!req.query.hasOwnProperty('limit')) return next(ErrorResponse.limitNotFound);
  if(!req.query.hasOwnProperty('offset')) return next(ErrorResponse.offsetNotFound);
  next();
};

module.exports.getRestuaruants = getRestuaruants;
module.exports.validateQueryParams = validateQueryParams;