var mongoose = require('mongoose');

var foursquareSchema = new mongoose.Schema({
  id: String,
  name: String,
  location: {
    zip_code: String,
    latitude: Number,
    longitude: Number,
    formattedAddress: String
  }
});

// Mongoose model
var foursquares = mongoose.model('foursquares', foursquareSchema);

module.exports.foursquares = foursquares;