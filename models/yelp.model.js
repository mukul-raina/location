var mongoose = require('mongoose');

var yelpSchema = new mongoose.Schema({
  name: String,
  url: String,
  phone: String,
  id: String,
  location:{
    zip_code: String,
    latitude: Number,
    longitude: Number,
    formattedAddress: String
  }
});

// Mongoose model
var yelps = mongoose.model('yelps', yelpSchema);

module.exports.yelps = yelps;