var mongoose = require('mongoose');

var dataSchema = new mongoose.Schema({
  name: String,
  location: {
    zip_code: String,
    latitude: Number,
    longitude: Number,
    formattedAddress: String,
    threeWordLocation: String
  }
});

// Mongoose model
var datas = mongoose.model('datas', dataSchema);

module.exports.datas = datas;